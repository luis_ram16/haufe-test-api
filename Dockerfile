FROM openjdk:8-jdk-alpine
MAINTAINER Luis Ramirez
COPY target/haufetest-0.0.1-SNAPSHOT.jar haufetest-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/haufetest-0.0.1-SNAPSHOT.jar"]