package com.luisramirez.haufetest.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Optional;

import com.luisramirez.haufetest.dto.ManufacturerDTO;
import com.luisramirez.haufetest.entity.ManufacturerEntity;
import com.luisramirez.haufetest.entity.ManufacturerEntity;
import com.luisramirez.haufetest.exception.notfound.ManufacturerNotFoundException;
import com.luisramirez.haufetest.exception.notupdated.ManufacturerNotUpdatedException;
import com.luisramirez.haufetest.repository.ManufacturerRepository;
import com.luisramirez.haufetest.repository.ManufacturerRepository;
import com.luisramirez.haufetest.service.impl.ManufacturerServiceImpl;
import com.luisramirez.haufetest.service.mapper.ManufacturerMapper;
import com.luisramirez.haufetest.service.mapper.ManufacturerMapper;
import com.luisramirez.haufetest.testutils.TestParent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@SpringBootTest
public class ManufacturerServiceTest {

    @Mock
    private ManufacturerRepository manufacturerRepository;

    @Mock
    private ManufacturerMapper manufacturerMapper;

    @Captor
    private ArgumentCaptor<ManufacturerEntity> manufacturerCaptor;

    @Autowired
    @InjectMocks
    private ManufacturerService manufacturerService = new ManufacturerServiceImpl();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllManufacturersTest() {
        Pageable pageable = PageRequest.of(0, 10,Sort.by(Sort.Direction.ASC,"name"));
        Mockito.when(manufacturerRepository.findAll((Pageable) Mockito.any())).thenReturn(new PageImpl<>(TestParent.getAllManufacturerEntities(), pageable, 10));

        Mockito.when(manufacturerMapper.toDTO((List<ManufacturerEntity>) Mockito.any())).thenReturn(TestParent.getAllManufacturerDTOs());
        Page<ManufacturerDTO> manufacturers = manufacturerService.getAllManufacturers(pageable);

        assertThat(manufacturers.getContent().size()).isEqualTo(2);
        assertThat(manufacturers.getContent().get(0).getName()).isEqualTo(TestParent.MANUFACTURER_1_NAME);
        assertThat(manufacturers.getContent().get(1).getName()).isEqualTo(TestParent.MANUFACTURER_2_NAME);
    }

    @Test
    public void createManufacturerTest() {
        ManufacturerDTO manufacturer = TestParent.getManufacturerDTOOne();
        ManufacturerEntity manufacturerEntity = TestParent.getManufacturerEntityOne();
        Mockito.when(manufacturerMapper.toEntity((ManufacturerDTO) Mockito.any())).thenReturn(manufacturerEntity);
        manufacturerService.deleteManufacturer(manufacturer.getId());
        manufacturerService.createManufacturer(manufacturer);

        Mockito.verify(manufacturerRepository).save(manufacturerCaptor.capture());
        assertThat(manufacturerCaptor.getValue().getName()).isEqualTo(manufacturer.getName());
        assertThat(manufacturerCaptor.getValue().getNationality().getName()).isEqualTo(manufacturer.getNationality().getName());
    }

    @Test
    public void getManufacturerTest() {
        ManufacturerEntity manufacturerEntity = TestParent.getManufacturerEntityOne();
        Mockito.when(manufacturerRepository.findById(Mockito.any())).thenReturn(Optional.of(manufacturerEntity));
        Mockito.when(manufacturerMapper.toDTO((ManufacturerEntity) Mockito.any())).thenReturn(TestParent.getManufacturerDTOOne());

        ManufacturerDTO manufacturer = manufacturerService.findManufacturerById(manufacturerEntity.getId());

        assertThat(manufacturer.getName()).isEqualTo(manufacturerEntity.getName());
        assertThat(manufacturer.getNationality().getName()).isEqualTo(manufacturerEntity.getNationality().getName());
    }

    @Test
    public void getManufacturerTestThrowExceptionOnNonExistingManufacturer() {
        Mockito.when(manufacturerRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(ManufacturerNotFoundException.class,
                () -> manufacturerService.findManufacturerById(TestParent.MANUFACTURER_1_ID));
    }

    @Test
    public void updateManufacturerTest() {
        ManufacturerDTO manufacturer = TestParent.getManufacturerDTOOne();
        ManufacturerEntity manufacturerEntity = TestParent.getManufacturerEntityOne();

        Mockito.when(manufacturerMapper.toEntity((ManufacturerDTO) Mockito.any())).thenReturn(manufacturerEntity);
        Mockito.when(manufacturerRepository.findById(Mockito.any())).thenReturn(Optional.of(manufacturerEntity));
        Mockito.when(manufacturerMapper.toDTO((ManufacturerEntity) Mockito.any())).thenReturn(manufacturer);

        manufacturerService.updateManufacturer(manufacturerEntity.getId(), manufacturer);

        Mockito.verify(manufacturerRepository).save(manufacturerCaptor.capture());
        assertThat(manufacturerCaptor.getValue().getName()).isEqualTo(manufacturerEntity.getName());
        assertThat(manufacturerCaptor.getValue().getNationality().getName()).isEqualTo(manufacturerEntity.getNationality().getName());
    }

    @Test
    public void updateManufacturerTestThrowExceptionOnNonExistingManufacturer() {
        ManufacturerDTO manufacturer = TestParent.getManufacturerDTOOne();
        ManufacturerEntity manufacturerEntity = TestParent.getManufacturerEntityOne();

        Mockito.when(manufacturerMapper.toEntity((ManufacturerDTO) Mockito.any())).thenReturn(manufacturerEntity);
        Mockito.when(manufacturerRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        
        assertThrows(ManufacturerNotUpdatedException.class,
                () -> manufacturerService.updateManufacturer(manufacturerEntity.getId(), manufacturer)
        );
    }

    @Test
    public void deleteManufacturerTest() {

        manufacturerService.deleteManufacturer(TestParent.MANUFACTURER_1_ID);

        Mockito.verify(manufacturerRepository).deleteById(TestParent.MANUFACTURER_1_ID);
    }

    @Test
    public void deleteManufacturerTestNotExistingManufacturer() {

        manufacturerService.deleteManufacturer(10);

        Mockito.verify(manufacturerRepository).deleteById(10);
    }

}
