package com.luisramirez.haufetest.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Optional;

import com.luisramirez.haufetest.dto.BeerTypeDTO;
import com.luisramirez.haufetest.entity.BeerTypeEntity;
import com.luisramirez.haufetest.exception.notfound.BeerTypeNotFoundException;
import com.luisramirez.haufetest.exception.notupdated.BeerTypeNotUpdatedException;
import com.luisramirez.haufetest.repository.BeerTypeRepository;
import com.luisramirez.haufetest.service.impl.BeerTypeServiceImpl;
import com.luisramirez.haufetest.service.mapper.BeerTypeMapper;
import com.luisramirez.haufetest.testutils.TestParent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@SpringBootTest
public class BeerTypeServiceTest {

    @Mock
    private BeerTypeRepository beerTypeRepository;

    @Mock
    private BeerTypeMapper beerTypeMapper;

    @Captor
    private ArgumentCaptor<BeerTypeEntity> beerTypeCaptor;

    @Autowired
    @InjectMocks
    private BeerTypeService beerTypeService = new BeerTypeServiceImpl();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllBeerTypesTest() {
        Pageable pageable = PageRequest.of(0, 10,Sort.by(Sort.Direction.ASC,"name"));
        Mockito.when(beerTypeRepository.findAll((Pageable) Mockito.any())).thenReturn(new PageImpl<>(TestParent.getAllBeerTypeEntities(), pageable, 2));

        Mockito.when(beerTypeMapper.toDTO((List< BeerTypeEntity>) Mockito.any())).thenReturn(TestParent.getAllBeerTypeDTOs());
        Page<BeerTypeDTO> beerTypes = beerTypeService.getAllBeerTypes(pageable);

        assertThat(beerTypes.getContent().size()).isEqualTo(3);
        assertThat(beerTypes.getContent().get(0).getName()).isEqualTo(TestParent.BEER_TYPE_1_NAME);
        assertThat(beerTypes.getContent().get(1).getName()).isEqualTo(TestParent.BEER_TYPE_2_NAME);
        assertThat(beerTypes.getContent().get(2).getName()).isEqualTo(TestParent.BEER_TYPE_3_NAME);
    }

    @Test
    public void createBeerTypeTest() {
        BeerTypeDTO beerType = TestParent.getBeerTypeDTOOne();
        BeerTypeEntity beerTypeEntity = TestParent.getBeerTypeEntityOne();
        Mockito.when(beerTypeMapper.toEntity((BeerTypeDTO) Mockito.any())).thenReturn(beerTypeEntity);
        beerTypeService.deleteBeerType(beerType.getId());
        beerTypeService.createBeerType(beerType);

        Mockito.verify(beerTypeRepository).save(beerTypeCaptor.capture());
        assertThat(beerTypeCaptor.getValue().getName()).isEqualTo(beerType.getName());
    }

    @Test
    public void getBeerTypeTest() {
        BeerTypeEntity beerTypeEntity = TestParent.getBeerTypeEntityOne();
        Mockito.when(beerTypeRepository.findById(Mockito.any())).thenReturn(Optional.of(beerTypeEntity));
        Mockito.when(beerTypeMapper.toDTO((BeerTypeEntity) Mockito.any())).thenReturn(TestParent.getBeerTypeDTOOne());

        BeerTypeDTO beerType = beerTypeService.findBeerTypeById(beerTypeEntity.getId());

        assertThat(beerType.getName()).isEqualTo(beerTypeEntity.getName());
    }

    @Test
    public void getBeerTypeTestThrowExceptionOnNonExistingBeerType() {
        Mockito.when(beerTypeRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(BeerTypeNotFoundException.class,
                () -> beerTypeService.findBeerTypeById(TestParent.BEER_TYPE_1_ID));
    }

    @Test
    public void updateBeerTypeTest() {
        BeerTypeDTO beerType = TestParent.getBeerTypeDTOOne();
        BeerTypeEntity beerTypeEntity = TestParent.getBeerTypeEntityOne();

        Mockito.when(beerTypeMapper.toEntity((BeerTypeDTO) Mockito.any())).thenReturn(beerTypeEntity);
        Mockito.when(beerTypeRepository.findById(Mockito.any())).thenReturn(Optional.of(beerTypeEntity));
        Mockito.when(beerTypeMapper.toDTO((BeerTypeEntity) Mockito.any())).thenReturn(beerType);

        beerTypeService.updateBeerType(beerTypeEntity.getId(), beerType);

        Mockito.verify(beerTypeRepository).save(beerTypeCaptor.capture());
        assertThat(beerTypeCaptor.getValue().getName()).isEqualTo(beerTypeEntity.getName());
    }

    @Test
    public void updateBeerTypeTestThrowExceptionOnNonExistingBeerType() {
        BeerTypeDTO beerType = TestParent.getBeerTypeDTOOne();
        BeerTypeEntity beerTypeEntity = TestParent.getBeerTypeEntityOne();

        Mockito.when(beerTypeMapper.toEntity((BeerTypeDTO) Mockito.any())).thenReturn(beerTypeEntity);
        Mockito.when(beerTypeRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(BeerTypeNotUpdatedException.class,
                () -> beerTypeService.updateBeerType(beerTypeEntity.getId(), beerType)
        );
    }

    @Test
    public void deleteBeerTypeTest() {

        beerTypeService.deleteBeerType(TestParent.BEER_TYPE_1_ID);

        Mockito.verify(beerTypeRepository).deleteById(TestParent.BEER_TYPE_1_ID);
    }

    @Test
    public void deleteBeerTypeTestNotExistingBeerType() {

        beerTypeService.deleteBeerType(10);

        Mockito.verify(beerTypeRepository).deleteById(10);
    }

}
