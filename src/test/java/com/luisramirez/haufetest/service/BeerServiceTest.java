package com.luisramirez.haufetest.service;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.luisramirez.haufetest.dto.BeerDTO;
import com.luisramirez.haufetest.dto.FilterDTO;
import com.luisramirez.haufetest.entity.BeerEntity;
import com.luisramirez.haufetest.exception.notfound.BeerNotFoundException;
import com.luisramirez.haufetest.exception.notupdated.BeerNotUpdatedException;
import com.luisramirez.haufetest.repository.BeerCustomRepository;
import com.luisramirez.haufetest.repository.BeerRepository;
import com.luisramirez.haufetest.service.impl.BeerServiceImpl;
import com.luisramirez.haufetest.service.mapper.BeerMapper;
import com.luisramirez.haufetest.testutils.TestParent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class BeerServiceTest {

    @Mock
    private BeerRepository beerRepository;

    @Mock
    private BeerCustomRepository beerCustomRepository;

    @Mock
    private BeerMapper beerMapper;

    @Captor
    private ArgumentCaptor<BeerEntity> beerCaptor;

    @Autowired
    @InjectMocks
    private BeerService beerService = new BeerServiceImpl();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllBeersTest() throws IOException {
        Pageable pageable = PageRequest.of(0, 10,Sort.by(Sort.Direction.ASC,"name"));
        Mockito.when(beerCustomRepository.findByFilter(Mockito.any(), Mockito.any()))
               .thenReturn(new PageImpl<>(TestParent.getAllBeerEntities(), pageable, 3));

        Mockito.when(beerMapper.toDTO((List< BeerEntity>) Mockito.any())).thenReturn(TestParent.getAllBeerDTOs());
        Page<BeerDTO> beers = beerService.getAllBeers(pageable, new FilterDTO());

        assertThat(beers.getContent().size()).isEqualTo(3);
        assertThat(beers.getContent().get(0).getName()).isEqualTo(TestParent.BEER_1_NAME);
        assertThat(beers.getContent().get(1).getName()).isEqualTo(TestParent.BEER_2_NAME);
        assertThat(beers.getContent().get(2).getName()).isEqualTo(TestParent.BEER_3_NAME);
    }

    @Test
    public void createBeerTest() throws IOException {
        BeerDTO beer = TestParent.getBeerDTOOne();
        BeerEntity beerEntity = TestParent.getBeerEntityOne();
        Mockito.when(beerMapper.toEntity((BeerDTO) Mockito.any())).thenReturn(beerEntity);
        beerService.createBeer(beer,beer.getManufacturer().getName());

        Mockito.verify(beerRepository).save(beerCaptor.capture());
        assertThat(beerCaptor.getValue().getName()).isEqualTo(beer.getName());
        assertThat(beerCaptor.getValue().getGraduation()).isEqualTo(beer.getGraduation());
        assertThat(beerCaptor.getValue().getType().getName()).isEqualTo(beer.getType().getName());
        assertThat(beerCaptor.getValue().getDescription()).isEqualTo(beer.getDescription());
        assertThat(beerCaptor.getValue().getManufacturer().getName()).isEqualTo(beer.getManufacturer().getName());
    }

    @Test
    public void getBeerTest() throws IOException {
        BeerEntity beerEntity = TestParent.getBeerEntityOne();
        Mockito.when(beerRepository.findById(Mockito.any())).thenReturn(Optional.of(beerEntity));
        Mockito.when(beerMapper.toDTO((BeerEntity) Mockito.any())).thenReturn(TestParent.getBeerDTOOne());

        BeerDTO beer = beerService.findBeerById(beerEntity.getId());

        assertThat(beer.getName()).isEqualTo(beerEntity.getName());
        assertThat(beer.getGraduation()).isEqualTo(beerEntity.getGraduation());
        assertThat(beer.getType().getName()).isEqualTo(beerEntity.getType().getName());
        assertThat(beer.getDescription()).isEqualTo(beerEntity.getDescription());
        assertThat(beer.getManufacturer().getName()).isEqualTo(beerEntity.getManufacturer().getName());
    }

    @Test
    public void getBeerTestThrowExceptionOnNonExistingBeer() {
        Mockito.when(beerRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(BeerNotFoundException.class,
                () -> beerService.findBeerById(TestParent.BEER_1_ID));
    }

    @Test
    public void updateBeerTest() throws IOException {
        BeerDTO beer = TestParent.getBeerDTOOne();
        BeerEntity beerEntity = TestParent.getBeerEntityOne();

        Mockito.when(beerMapper.toEntity((BeerDTO) Mockito.any())).thenReturn(beerEntity);
        Mockito.when(beerRepository.findById(Mockito.any())).thenReturn(Optional.of(beerEntity));
        Mockito.when(beerMapper.toDTO((BeerEntity) Mockito.any())).thenReturn(beer);

        beerService.updateBeer(beerEntity.getId(), beer, beer.getManufacturer().getName());

        Mockito.verify(beerRepository).save(beerCaptor.capture());
        assertThat(beerCaptor.getValue().getName()).isEqualTo(beerEntity.getName());
        assertThat(beerCaptor.getValue().getGraduation()).isEqualTo(beerEntity.getGraduation());
        assertThat(beerCaptor.getValue().getType().getName()).isEqualTo(beerEntity.getType().getName());
        assertThat(beerCaptor.getValue().getDescription()).isEqualTo(beerEntity.getDescription());
        assertThat(beerCaptor.getValue().getManufacturer().getName()).isEqualTo(beerEntity.getManufacturer().getName());

    }

    @Test
    public void updateBeerTestThrowExceptionOnNonExistingBeer() {
        BeerDTO beer = TestParent.getBeerDTOOne();
        BeerEntity beerEntity = TestParent.getBeerEntityOne();

        Mockito.when(beerMapper.toEntity((BeerDTO) Mockito.any())).thenReturn(beerEntity);
        Mockito.when(beerRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(BeerNotUpdatedException.class,
                () -> beerService.updateBeer(beerEntity.getId(), beer, beer.getManufacturer().getName())
        );
    }

    @Test
    public void deleteBeerTest() throws AccessDeniedException {
        Mockito.when(beerRepository.findById(Mockito.any())).thenReturn(Optional.of(TestParent.getBeerEntityOne()));

        beerService.deleteBeer(TestParent.BEER_1_ID, TestParent.MANUFACTURER_1_NAME);

        Mockito.verify(beerRepository).deleteById(TestParent.BEER_1_ID);
    }

    @Test
    public void deleteBeerTestNotExistingBeer() throws AccessDeniedException {

        assertThrows(BeerNotFoundException.class,
                () ->   beerService.deleteBeer(10, TestParent.MANUFACTURER_1_NAME)
        );

    }

}
