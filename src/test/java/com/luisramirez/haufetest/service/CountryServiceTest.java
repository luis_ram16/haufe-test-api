package com.luisramirez.haufetest.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Optional;

import com.luisramirez.haufetest.dto.CountryDTO;
import com.luisramirez.haufetest.entity.CountryEntity;
import com.luisramirez.haufetest.exception.notfound.CountryNotFoundException;
import com.luisramirez.haufetest.exception.notupdated.CountryNotUpdatedException;
import com.luisramirez.haufetest.repository.CountryRepository;
import com.luisramirez.haufetest.service.impl.CountryServiceImpl;
import com.luisramirez.haufetest.service.mapper.CountryMapper;
import com.luisramirez.haufetest.testutils.TestParent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@SpringBootTest
public class CountryServiceTest {

    @Mock
    private CountryRepository countryRepository;

    @Mock
    private CountryMapper countryMapper;

    @Captor
    private ArgumentCaptor<CountryEntity> countryCaptor;

    @Autowired
    @InjectMocks
    private CountryService countryService = new CountryServiceImpl();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllCountriesTest() {
        Pageable pageable = PageRequest.of(0, 10,Sort.by(Sort.Direction.ASC,"name"));
        Mockito.when(countryRepository.findAll((Pageable) Mockito.any())).thenReturn(new PageImpl<>(TestParent.getAllCountryEntities(), pageable, 10));

        Mockito.when(countryMapper.toDTO((List< CountryEntity>) Mockito.any())).thenReturn(TestParent.getAllCountryDTOs());
        Page<CountryDTO> countrys = countryService.getAllCountries(pageable);

        assertThat(countrys.getContent().size()).isEqualTo(2);
        assertThat(countrys.getContent().get(0).getName()).isEqualTo(TestParent.COUNTRY_1_NAME);
        assertThat(countrys.getContent().get(1).getName()).isEqualTo(TestParent.COUNTRY_2_NAME);
    }

    @Test
    public void createCountryTest() {
        CountryDTO country = TestParent.getCountryDTOOne();
        CountryEntity countryEntity = TestParent.getCountryEntityOne();
        Mockito.when(countryMapper.toEntity((CountryDTO) Mockito.any())).thenReturn(countryEntity);
        countryService.deleteCountry(country.getId());
        countryService.createCountry(country);

        Mockito.verify(countryRepository).save(countryCaptor.capture());
        assertThat(countryCaptor.getValue().getName()).isEqualTo(country.getName());
    }

    @Test
    public void getCountryTest() {
        CountryEntity countryEntity = TestParent.getCountryEntityOne();
        Mockito.when(countryRepository.findById(Mockito.any())).thenReturn(Optional.of(countryEntity));
        Mockito.when(countryMapper.toDTO((CountryEntity) Mockito.any())).thenReturn(TestParent.getCountryDTOOne());

        CountryDTO country = countryService.findCountryById(countryEntity.getId());

        assertThat(country.getName()).isEqualTo(countryEntity.getName());
    }

    @Test
    public void getCountryTestThrowExceptionOnNonExistingCountry() {
        Mockito.when(countryRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(CountryNotFoundException.class,
                () -> countryService.findCountryById(TestParent.COUNTRY_1_ID));
    }

    @Test
    public void updateCountryTest() {
        CountryDTO country = TestParent.getCountryDTOOne();
        CountryEntity countryEntity = TestParent.getCountryEntityOne();

        Mockito.when(countryMapper.toEntity((CountryDTO) Mockito.any())).thenReturn(countryEntity);
        Mockito.when(countryRepository.findById(Mockito.any())).thenReturn(Optional.of(countryEntity));
        Mockito.when(countryMapper.toDTO((CountryEntity) Mockito.any())).thenReturn(country);

        countryService.updateCountry(countryEntity.getId(), country);

        Mockito.verify(countryRepository).save(countryCaptor.capture());
        assertThat(countryCaptor.getValue().getName()).isEqualTo(countryEntity.getName());

    }

    @Test
    public void updateCountryTestThrowExceptionOnNonExistingCountry() {
        CountryDTO country = TestParent.getCountryDTOOne();
        CountryEntity countryEntity = TestParent.getCountryEntityOne();

        Mockito.when(countryMapper.toEntity((CountryDTO) Mockito.any())).thenReturn(countryEntity);
        Mockito.when(countryRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(CountryNotUpdatedException.class,
                () -> countryService.updateCountry(countryEntity.getId(), country)
        );
    }

    @Test
    public void deleteCountryTest() {

        countryService.deleteCountry(TestParent.COUNTRY_1_ID);

        Mockito.verify(countryRepository).deleteById(TestParent.COUNTRY_1_ID);
    }

    @Test
    public void deleteCountryTestNotExistingCountry() {

        countryService.deleteCountry(10);

        Mockito.verify(countryRepository).deleteById(10);
    }

}
