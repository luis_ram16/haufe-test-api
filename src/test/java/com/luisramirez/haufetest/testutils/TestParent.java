package com.luisramirez.haufetest.testutils;

import java.util.Arrays;
import java.util.List;

import com.luisramirez.haufetest.dto.BeerDTO;
import com.luisramirez.haufetest.dto.BeerTypeDTO;
import com.luisramirez.haufetest.dto.CountryDTO;
import com.luisramirez.haufetest.dto.ManufacturerDTO;
import com.luisramirez.haufetest.entity.BeerEntity;
import com.luisramirez.haufetest.entity.BeerTypeEntity;
import com.luisramirez.haufetest.entity.CountryEntity;
import com.luisramirez.haufetest.entity.ManufacturerEntity;
import com.luisramirez.haufetest.service.mapper.BeerMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class TestParent {

    /*      COUNTRIES     */

    public static final Integer COUNTRY_1_ID = 1;
    public static final String COUNTRY_1_NAME = "venezuela";

    public static final Integer COUNTRY_2_ID = 2;
    public static final String COUNTRY_2_NAME = "paises bajos";

    /*      BEER TYPES      */

    public static final Integer BEER_TYPE_1_ID = 1;
    public static final String BEER_TYPE_1_NAME = "pilsen";

    public static final Integer BEER_TYPE_2_ID = 2;
    public static final String BEER_TYPE_2_NAME = "helles";

    public static final Integer BEER_TYPE_3_ID = 3;
    public static final String BEER_TYPE_3_NAME = "pale lager";

    /*      MANUFACTURERS       */

    public static final Integer MANUFACTURER_1_ID = 1;
    public static final String MANUFACTURER_1_NAME = "polar";

    public static final Integer MANUFACTURER_2_ID = 2;
    public static final String MANUFACTURER_2_NAME = "heineken";

    /*      BEERS               */

    public static final Integer BEER_1_ID = 1;
    public static final String BEER_1_NAME = "polar pilsen";
    public static final float BEER_1_GRADUATION = 4.5f;
    public static final String BEER_1_DESCRIPTION = "Color dorado y su espuma blanca. Aroma ligero a malta y maíz. Sabor con cuerpo ligero, algo dulce.";

    public static final Integer BEER_2_ID = 2;
    public static final String BEER_2_NAME = "solera regular";
    public static final float BEER_2_GRADUATION = 6f;
    public static final String BEER_2_DESCRIPTION = "Conocida por su carácter fuerte y cuerpo pronunciado, lleva 6° de grados de alcohol y su habitual tono "
            + "amarillo intenso que la convierten en la cerveza clásica del portafolio.";

    public static final Integer BEER_3_ID = 3;
    public static final String BEER_3_NAME = "heineken lager";
    public static final float BEER_3_GRADUATION = 5f;
    public static final String BEER_3_DESCRIPTION = "It is brewed only with purified water, malted barley, hops and importantly Heineken A-yeast, "
            + "a proprietary yeast strain developed by H. Elion in 1886.";

    /*      COUNTRIES     */

    public static CountryEntity getCountryEntityOne() {
        return new CountryEntity(COUNTRY_1_ID, COUNTRY_1_NAME);
    }

    public static CountryEntity getCountryEntityTwo() {
        return new CountryEntity(COUNTRY_2_ID, COUNTRY_2_NAME);
    }

    public static CountryDTO getCountryDTOOne() {
        return new CountryDTO(COUNTRY_1_ID, COUNTRY_1_NAME);
    }

    public static CountryDTO getCountryDTOTwo() {
        return new CountryDTO(COUNTRY_2_ID, COUNTRY_2_NAME);
    }

    public static List<CountryEntity> getAllCountryEntities() {
        return Arrays.asList(
                getCountryEntityOne(),
                getCountryEntityTwo()
        );
    }

    public static List<CountryDTO> getAllCountryDTOs() {
        return Arrays.asList(
                getCountryDTOOne(),
                getCountryDTOTwo()
        );
    }

    /*      BEER TYPES      */

    public static BeerTypeEntity getBeerTypeEntityOne() {
        return new BeerTypeEntity(BEER_TYPE_1_ID, BEER_TYPE_1_NAME);
    }

    public static BeerTypeEntity getBeerTypeEntityTwo() {
        return new BeerTypeEntity(BEER_TYPE_2_ID, BEER_TYPE_2_NAME);
    }

    public static BeerTypeEntity getBeerTypeEntityThree() {
        return new BeerTypeEntity(BEER_TYPE_3_ID, BEER_TYPE_3_NAME);
    }

    public static BeerTypeDTO getBeerTypeDTOOne() {
        return new BeerTypeDTO(BEER_TYPE_1_ID, BEER_TYPE_1_NAME);
    }

    public static BeerTypeDTO getBeerTypeDTOTwo() {
        return new BeerTypeDTO(BEER_TYPE_2_ID, BEER_TYPE_2_NAME);
    }

    public static BeerTypeDTO getBeerTypeDTOThree() {
        return new BeerTypeDTO(BEER_TYPE_3_ID, BEER_TYPE_3_NAME);
    }

    public static List<BeerTypeEntity> getAllBeerTypeEntities() {
        return Arrays.asList(
                getBeerTypeEntityOne(),
                getBeerTypeEntityTwo(),
                getBeerTypeEntityThree()
        );
    }

    public static List<BeerTypeDTO> getAllBeerTypeDTOs() {
        return Arrays.asList(
                getBeerTypeDTOOne(),
                getBeerTypeDTOTwo(),
                getBeerTypeDTOThree()
        );
    }

    /*      MANUFACTURERS       */

    public static ManufacturerEntity getManufacturerEntityOne() {
        return new ManufacturerEntity(MANUFACTURER_1_ID, MANUFACTURER_1_NAME, getCountryEntityOne());
    }
    public static ManufacturerEntity getManufacturerEntityTwo() {
        return new ManufacturerEntity(MANUFACTURER_2_ID, MANUFACTURER_2_NAME, getCountryEntityTwo());
    }

    public static ManufacturerDTO getManufacturerDTOOne() {
        return new ManufacturerDTO(MANUFACTURER_1_ID, MANUFACTURER_1_NAME, getCountryDTOOne());
    }

    public static ManufacturerDTO getManufacturerDTOTwo() {
        return new ManufacturerDTO(MANUFACTURER_2_ID, MANUFACTURER_2_NAME, getCountryDTOTwo());
    }

    public static List<ManufacturerEntity> getAllManufacturerEntities() {
        return Arrays.asList(
                getManufacturerEntityOne(),
                getManufacturerEntityTwo()
        );
    }

    public static List<ManufacturerDTO> getAllManufacturerDTOs() {
        return Arrays.asList(
                getManufacturerDTOOne(),
                getManufacturerDTOTwo()
        );
    }

    /*      BEERS               */

    public static BeerEntity getBeerEntityOne() {
        return new BeerEntity(BEER_1_ID, BEER_1_NAME, BEER_1_GRADUATION, getBeerTypeEntityOne(), BEER_1_DESCRIPTION, getManufacturerEntityOne());
    }

    public static BeerEntity getBeerEntityTwo() {
        return new BeerEntity(BEER_2_ID, BEER_2_NAME, BEER_2_GRADUATION, getBeerTypeEntityTwo(), BEER_2_DESCRIPTION, getManufacturerEntityOne());
    }

    public static BeerEntity getBeerEntityThree() {
        return new BeerEntity(BEER_3_ID, BEER_3_NAME, BEER_3_GRADUATION, getBeerTypeEntityThree(), BEER_3_DESCRIPTION, getManufacturerEntityTwo());
    }

    public static BeerDTO getBeerDTOOne() {
        return new BeerDTO(BEER_1_ID, BEER_1_NAME, BEER_1_GRADUATION, getBeerTypeDTOOne(), BEER_1_DESCRIPTION, getManufacturerDTOOne());
    }

    public static BeerDTO getBeerDTOTwo() {
        return new BeerDTO(BEER_2_ID, BEER_2_NAME, BEER_2_GRADUATION, getBeerTypeDTOTwo(), BEER_2_DESCRIPTION, getManufacturerDTOOne());
    }

    public static BeerDTO getBeerDTOThree() {
        return new BeerDTO(BEER_3_ID, BEER_3_NAME, BEER_3_GRADUATION, getBeerTypeDTOThree(), BEER_3_DESCRIPTION, getManufacturerDTOTwo());
    }

    public static List<BeerEntity> getAllBeerEntities() {
        return Arrays.asList(
                getBeerEntityOne(),
                getBeerEntityTwo(),
                getBeerEntityThree()
        );
    }

    public static List<BeerDTO> getAllBeerDTOs() {
        return Arrays.asList(
                getBeerDTOOne(),
                getBeerDTOTwo(),
                getBeerDTOThree()
        );
    }

}
