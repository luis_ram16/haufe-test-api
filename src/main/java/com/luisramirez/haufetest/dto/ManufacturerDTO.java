package com.luisramirez.haufetest.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Manufacturer", description = "Contains all data of the manufacturer.<br><br>")
public class ManufacturerDTO {

    @ApiModelProperty(notes = "Id of the manufacturer.<br>", dataType = "Integer", example = "1")
    private int id;

    @ApiModelProperty(notes = "Name of the manufacturer.<br>", dataType = "String", example = "1")
    private String name;

    @ApiModelProperty(notes = "Nationality of the manufacturer.<br>", dataType = "CountryDTO", example = "1")
    private CountryDTO nationality;

}
