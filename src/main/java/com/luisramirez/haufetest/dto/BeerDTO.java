package com.luisramirez.haufetest.dto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Beer", description = "Contains all data of the beer.<br><br>")
public class BeerDTO {

    @ApiModelProperty(notes = "Id of the beer.<br>", dataType = "Integer", example = "1")
    private int id;

    @ApiModelProperty(notes = "Name of the beer.<br>", dataType = "String", example = "polar pilsen")
    private String name;

    @ApiModelProperty(notes = "Graduation of the beer.<br>", dataType = "Float", example = "4.5")
    private float graduation;

    @ApiModelProperty(notes = "Type of the beer.<br>", dataType = "BeerTypeDTO")
    private BeerTypeDTO type;

    @ApiModelProperty(notes = "Description of the beer.<br>", dataType = "String",
            example = "Color dorado y su espuma blanca. Aroma ligero a malta y maíz. Sabor con cuerpo ligero, algo dulce")
    private String description;

    @ApiModelProperty(notes = "Manufacturer of the beer.<br>", dataType = "ManufacturerDTO")
    private ManufacturerDTO manufacturer;

    @ApiModelProperty(hidden = true)
    private byte[] image;

    @ApiModelProperty(hidden = true)
    private String externalImageUrl;

    public BeerDTO(Integer id, String name, float graduation, BeerTypeDTO type, String description, ManufacturerDTO manufacturer) {
        this.id = id;
        this.name = name;
        this.graduation = graduation;
        this.type = type;
        this.description = description;
        this.manufacturer = manufacturer;
    }

    public void setImage(String imageRoute) throws IOException {
        if (imageRoute != null && imageRoute != "") {
            this.image = Files.readAllBytes(Paths.get(imageRoute));
        }
    }

}
