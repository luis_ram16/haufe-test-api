package com.luisramirez.haufetest.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Country", description = "Contains all data of the country.<br><br>")
public class CountryDTO {

    @ApiModelProperty(notes = "Id of the country.<br>", dataType = "Integer", example = "1")
    private int id;

    @ApiModelProperty(notes = "Name of the country.<br>", dataType = "String", example = "venezuela")
    private String name;

}
