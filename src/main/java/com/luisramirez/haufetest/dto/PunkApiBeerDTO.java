package com.luisramirez.haufetest.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PunkApiBeerDTO {

    private String name;
    private String description;
    private Float abv;
    private String image_url;
}
