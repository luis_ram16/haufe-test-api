package com.luisramirez.haufetest.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class FilterDTO {

    private String name;
    private Float graduation;
    private String typeName;
    private String manufacturerName;
    private String nationalityName;

}
