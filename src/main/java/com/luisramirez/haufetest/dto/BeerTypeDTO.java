package com.luisramirez.haufetest.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Beer Type", description = "Contains all data of the beer type.<br><br>")
public class BeerTypeDTO {

    @ApiModelProperty(notes = "Id of the beer type.<br>", dataType = "Integer", example = "1")
    private int id;

    @ApiModelProperty(notes = "Name of the beer type.<br>", dataType = "String", example = "pilsen")
    private String name;

}
