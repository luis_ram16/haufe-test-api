package com.luisramirez.haufetest.controller;

import java.util.ArrayList;
import java.util.List;

import com.luisramirez.haufetest.dto.ManufacturerDTO;
import com.luisramirez.haufetest.service.ManufacturerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Api(tags = "Manufacturer")
public class ManufacturerController {

    @Autowired
    private ManufacturerService manufacturerService;

    @PostMapping("/api/manufacturer")
    @ApiOperation(value = "Create a manufacturer and return it.", response = ManufacturerDTO.class)
    public ResponseEntity<ManufacturerDTO> createManufacturer(@RequestBody ManufacturerDTO manufacturer) {
        manufacturer = manufacturerService.createManufacturer(manufacturer);
        return ResponseEntity.status(HttpStatus.CREATED).body(manufacturer);
    }

    @GetMapping("/api/manufacturers")
    @ApiOperation(value = "Get all manufacturers.", response = ManufacturerDTO.class)
    public ResponseEntity<Page<ManufacturerDTO>> getAllManufacturers(
            @RequestParam(defaultValue = "name") String sortBy,
            @RequestParam(defaultValue = "ASC") Sort.Direction sortDirection,
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int elementsPerPage) {
        Pageable pageable = PageRequest.of(pageNumber, elementsPerPage, Sort.by(sortDirection,sortBy));
        Page<ManufacturerDTO> manufacturers = manufacturerService.getAllManufacturers(pageable);
        return ResponseEntity.status(HttpStatus.OK).body(manufacturers);
    }

    @GetMapping("/api/manufacturer/{id}")
    @ApiOperation(value = "Find a manufacturer given his id.", response = ManufacturerDTO.class)
    public ResponseEntity<ManufacturerDTO> findManufacturer(@PathVariable int id) {
        ManufacturerDTO manufacturer = manufacturerService.findManufacturerById(id);
        return ResponseEntity.status(HttpStatus.OK).body(manufacturer);
    }

    @PutMapping("/api/manufacturer/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update a manufacturer given his id.")
    public void updateManufacturer(@PathVariable int id, @RequestBody ManufacturerDTO manufacturer) {
        manufacturerService.updateManufacturer(id, manufacturer);
    }

    @DeleteMapping("/api/manufacturer/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete a manufacturer given his id.")
    public void deleteManufacturer(@PathVariable int id) {
        manufacturerService.deleteManufacturer(id);
    }
}
