package com.luisramirez.haufetest.controller;


import com.luisramirez.haufetest.dto.BeerTypeDTO;
import com.luisramirez.haufetest.dto.ManufacturerDTO;
import com.luisramirez.haufetest.service.BeerTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Api(tags = "Beer Type")
public class BeerTypeController {

    @Autowired
    private BeerTypeService beerTypeService;

    @PostMapping("/api/beer/type")
    @ApiOperation(value = "Create a beer type and return it.", response = BeerTypeDTO.class)
    public ResponseEntity<BeerTypeDTO> createBeerType(@RequestBody BeerTypeDTO beerType) {
        beerType = beerTypeService.createBeerType(beerType);
        return ResponseEntity.status(HttpStatus.CREATED).body(beerType);
    }

    @GetMapping("/api/beer/types")
    @ApiOperation(value = "Get all beer types.", response = BeerTypeDTO.class)
    public ResponseEntity<Page<BeerTypeDTO>> getAllBeerTypes(
            @RequestParam(defaultValue = "name") String sortBy,
            @RequestParam(defaultValue = "ASC") Sort.Direction sortDirection,
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int elementsPerPage) {
        Pageable pageable = PageRequest.of(pageNumber, elementsPerPage, Sort.by(sortDirection,sortBy));
        Page<BeerTypeDTO> beerTypes = beerTypeService.getAllBeerTypes(pageable);
        return ResponseEntity.status(HttpStatus.OK).body(beerTypes);
    }

    @GetMapping("/api/beer/type/{id}")
    @ApiOperation(value = "Find a beer type given his id.", response = BeerTypeDTO.class)
    public ResponseEntity<BeerTypeDTO> findBeerType(@PathVariable int id) {
        BeerTypeDTO beerType = beerTypeService.findBeerTypeById(id);
        return ResponseEntity.status(HttpStatus.OK).body(beerType);
    }

    @PutMapping("/api/beer/type/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update a beer type given his id.")
    public void updateBeerType(@PathVariable int id, @RequestBody BeerTypeDTO beerType) {
        beerTypeService.updateBeerType(id, beerType);
    }

    @DeleteMapping("/api/beer/type/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete a beer type given his id.")
    public void deleteBeerType(@PathVariable int id) {
        beerTypeService.deleteBeerType(id);
    }

}
