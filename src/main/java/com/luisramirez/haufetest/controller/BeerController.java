package com.luisramirez.haufetest.controller;

import java.io.IOException;
import java.nio.file.AccessDeniedException;

import com.luisramirez.haufetest.dto.BeerDTO;
import com.luisramirez.haufetest.dto.FilterDTO;
import com.luisramirez.haufetest.service.BeerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Slf4j
@Api(tags = "Beer")
public class BeerController {

    @Autowired
    private BeerService beerService;

    @PostMapping("/api/beer")
    @ApiOperation(value = "Create a beer and return it.", response = BeerDTO.class)
    public ResponseEntity<BeerDTO> createBeer(
            @ApiParam(name = "beer", type = "BeerDTO")
            @RequestBody BeerDTO beer) throws IOException {

        String username = "admin";
        beer = beerService.createBeer(beer,username);
        return ResponseEntity.status(HttpStatus.CREATED).body(beer);
    }

    @GetMapping("/api/beers")
    @ApiOperation(value = "Get all beers or search by it's properties.", response = BeerDTO.class)
    public ResponseEntity<Page<BeerDTO>> getAllBeers(
            @RequestParam(defaultValue = "name") String sortBy,
            @RequestParam(defaultValue = "ASC") Sort.Direction sortDirection,
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int elementsPerPage,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String typeName,
            @RequestParam(required = false) Float graduation,
            @RequestParam(required = false) String manufacturerName,
            @RequestParam(required = false) String nationalityName
            ) throws IOException {

        FilterDTO filter = new FilterDTO(name, graduation, typeName, manufacturerName, nationalityName);
        Pageable pageable = PageRequest.of(pageNumber, elementsPerPage, Sort.by(sortDirection,sortBy));
        Page<BeerDTO> beers = beerService.getAllBeers(pageable,filter);
        return ResponseEntity.status(HttpStatus.OK).body(beers);
    }

    @GetMapping("/api/beer/{id}")
    @ApiOperation(value = "Find a manufacturer beer his id.", response = BeerDTO.class)
    public ResponseEntity<BeerDTO> findBeer(@PathVariable int id) throws IOException {
        BeerDTO beer = beerService.findBeerById(id);
        return ResponseEntity.status(HttpStatus.OK).body(beer);
    }

    @PutMapping("/api/beer/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update a beer given his id.")
    public void updateBeer(@PathVariable int id, @RequestBody BeerDTO beer) throws IOException {

        String username = "admin";
        beerService.updateBeer(id, beer, username);
    }

    @DeleteMapping("/api/beer/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete a beer given his id.")
    public void deleteBeer(@PathVariable int id) throws AccessDeniedException {
        String username = "admin";
        beerService.deleteBeer(id, username);
    }

    @PutMapping(value = "/api/beer/{id}/image")
    @ResponseBody
    @ApiOperation(value = "Upload beer image given his id.")
    public ResponseEntity<BeerDTO> uploadImage(
            @PathVariable int id,
            @RequestParam("file") MultipartFile beerImage) throws IOException {

        String username = "admin";
        beerService.uploadImage(id, beerImage, username);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
