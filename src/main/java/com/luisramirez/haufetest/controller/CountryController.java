package com.luisramirez.haufetest.controller;

import com.luisramirez.haufetest.dto.CountryDTO;
import com.luisramirez.haufetest.service.CountryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Api(tags = "Country")
public class CountryController {

    @Autowired
    private CountryService countryService;

    @PostMapping("/api/country")
    @ApiOperation(value = "Create a country and return it.", response = CountryDTO.class)
    public ResponseEntity<CountryDTO> createCountry(@RequestBody CountryDTO country) {
        country = countryService.createCountry(country);
        return ResponseEntity.status(HttpStatus.CREATED).body(country);
    }

    @GetMapping("/api/countries")
    @ApiOperation(value = "Get all country.", response = CountryDTO.class)
    public ResponseEntity<Page<CountryDTO>> getAllCountries(
            @RequestParam(defaultValue = "name") String sortBy,
            @RequestParam(defaultValue = "ASC") Sort.Direction sortDirection,
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int elementsPerPage) {
        Pageable pageable = PageRequest.of(pageNumber, elementsPerPage, Sort.by(sortDirection,sortBy));
        Page<CountryDTO> countries = countryService.getAllCountries(pageable);
        return ResponseEntity.status(HttpStatus.OK).body(countries);
    }

    @GetMapping("/api/country/{id}")
    @ApiOperation(value = "Find a country given his id.", response = CountryDTO.class)
    public ResponseEntity<CountryDTO> findCountry(@PathVariable int id) {
        CountryDTO country = countryService.findCountryById(id);
        return ResponseEntity.status(HttpStatus.OK).body(country);
    }

    @PutMapping("/api/country/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update a country given his id.")
    public void updateCountry(@PathVariable int id, @RequestBody CountryDTO country) {
        countryService.updateCountry(id,country);
    }

    @DeleteMapping("/api/country/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete a country given his id.")
    public void deleteCountry(@PathVariable int id) {
        countryService.deleteCountry(id);
    }

}
