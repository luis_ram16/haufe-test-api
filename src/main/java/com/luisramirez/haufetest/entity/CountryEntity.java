package com.luisramirez.haufetest.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "COUNTRY")
@Getter
@Setter
@AllArgsConstructor
@ToString
@Entity
@NoArgsConstructor
public class CountryEntity extends GenericEntity{

    public CountryEntity(Integer id, String name) {
        setId(id);
        setName(name);
    }

    @Column(name = "NAME", nullable = false)
    private String name;

    @OneToMany(mappedBy = "nationality", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ManufacturerEntity> manufacturers;

}
