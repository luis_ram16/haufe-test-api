package com.luisramirez.haufetest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "BEER")
@Getter
@Setter
@AllArgsConstructor
@ToString
@Entity
@NoArgsConstructor
public class BeerEntity extends GenericEntity{

    public BeerEntity(Integer id, String name, float graduation, BeerTypeEntity beerTypeEntity, String description, ManufacturerEntity manufacturerEntity) {
        setId(id);
        setName(name);
        setGraduation(graduation);
        setType(beerTypeEntity);
        setDescription(description);
        setManufacturer(manufacturerEntity);
    }

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "GRADUATION", nullable = false)
    private float graduation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BEER_TYPE_ID", nullable = false)
    private BeerTypeEntity type;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MANUFACTURER_ID", nullable = false)
    private ManufacturerEntity manufacturer;

    @Column(name = "IMAGE_ROUTE")
    private String imageRoute;

}
