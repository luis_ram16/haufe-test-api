package com.luisramirez.haufetest.exception;

import java.nio.file.AccessDeniedException;

import com.luisramirez.haufetest.exception.notfound.BeerNotFoundException;
import com.luisramirez.haufetest.exception.notfound.BeerTypeNotFoundException;
import com.luisramirez.haufetest.exception.notfound.CountryNotFoundException;
import com.luisramirez.haufetest.exception.notfound.ManufacturerNotFoundException;
import com.luisramirez.haufetest.exception.notupdated.BeerNotUpdatedException;
import com.luisramirez.haufetest.exception.notupdated.BeerTypeNotUpdatedException;
import com.luisramirez.haufetest.exception.notupdated.CountryNotUpdatedException;
import com.luisramirez.haufetest.exception.notupdated.ManufacturerNotUpdatedException;
import org.hibernate.PropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionAdvice {

    @ResponseBody
    @ExceptionHandler(BeerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String beerNotFoundHandler(BeerNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(ManufacturerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String manufacturerNotFoundHandler(ManufacturerNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(CountryNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String countryNotFoundHandler(CountryNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(BeerTypeNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String beerTypeNotFoundHandler(BeerTypeNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(BeerNotUpdatedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String beerNotUpdatedHandler(BeerNotUpdatedException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(BeerTypeNotUpdatedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String beerTypeNotUpdatedHandler(BeerTypeNotUpdatedException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(CountryNotUpdatedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String countryNotUpdatedHandler(CountryNotUpdatedException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(ManufacturerNotUpdatedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String manufacturerNotUpdatedHandler(ManufacturerNotUpdatedException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(PropertyValueException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String propertyNotValidHandler(PropertyValueException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN )
    String AccessDeniedHandler(AccessDeniedException ex) {
        return ex.getMessage();
    }

}
