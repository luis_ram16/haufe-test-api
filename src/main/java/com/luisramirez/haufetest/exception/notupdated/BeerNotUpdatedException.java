package com.luisramirez.haufetest.exception.notupdated;

public class BeerNotUpdatedException extends RuntimeException {

    public BeerNotUpdatedException(int id) {
        super("Could not update beer " + id);
    }
}