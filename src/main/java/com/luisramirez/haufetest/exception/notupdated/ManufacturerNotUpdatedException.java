package com.luisramirez.haufetest.exception.notupdated;

public class ManufacturerNotUpdatedException extends RuntimeException {

    public ManufacturerNotUpdatedException(int id) {

        super("Could not update manufacturer " + id);
    }
}