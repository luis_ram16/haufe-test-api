package com.luisramirez.haufetest.exception.notupdated;

public class BeerTypeNotUpdatedException extends RuntimeException {

    public BeerTypeNotUpdatedException(int id) {

        super("Could not update beer type " + id);
    }
}