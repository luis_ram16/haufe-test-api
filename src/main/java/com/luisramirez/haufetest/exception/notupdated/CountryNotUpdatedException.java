package com.luisramirez.haufetest.exception.notupdated;

public class CountryNotUpdatedException extends RuntimeException {

    public CountryNotUpdatedException(int id) {

        super("Could not update country " + id);
    }
}