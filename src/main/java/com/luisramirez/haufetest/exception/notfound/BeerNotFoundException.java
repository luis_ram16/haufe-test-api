package com.luisramirez.haufetest.exception.notfound;

public class BeerNotFoundException  extends RuntimeException {

    public BeerNotFoundException(int id) {
        super("Could not find beer " + id);
    }
}