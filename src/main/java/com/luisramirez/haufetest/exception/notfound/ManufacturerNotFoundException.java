package com.luisramirez.haufetest.exception.notfound;

public class ManufacturerNotFoundException extends RuntimeException {

    public ManufacturerNotFoundException(int id) {

        super("Could not find manufacturer " + id);
    }
}