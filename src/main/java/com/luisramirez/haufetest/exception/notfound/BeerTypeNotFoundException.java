package com.luisramirez.haufetest.exception.notfound;

public class BeerTypeNotFoundException extends RuntimeException {

    public BeerTypeNotFoundException(int id) {

        super("Could not find beer type " + id);
    }
}