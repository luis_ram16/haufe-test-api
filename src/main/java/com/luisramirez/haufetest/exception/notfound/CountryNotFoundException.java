package com.luisramirez.haufetest.exception.notfound;

public class CountryNotFoundException extends RuntimeException {

    public CountryNotFoundException(int id) {

        super("Could not find country " + id);
    }
}