package com.luisramirez.haufetest.service;

import java.util.List;

import com.luisramirez.haufetest.dto.ManufacturerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface ManufacturerService {


    ManufacturerDTO createManufacturer(ManufacturerDTO manufacturer);

    Page<ManufacturerDTO> getAllManufacturers(Pageable pageable);

    ManufacturerDTO findManufacturerById(int manufacturerId);

    ManufacturerDTO updateManufacturer(int manufacturerId, ManufacturerDTO manufacturer);

    void deleteManufacturer(int manufacturerId);

}
