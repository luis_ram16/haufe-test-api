package com.luisramirez.haufetest.service;

import java.util.List;

import com.luisramirez.haufetest.dto.CountryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface CountryService {


    CountryDTO createCountry(CountryDTO country);

    Page<CountryDTO> getAllCountries(Pageable pageable);

    CountryDTO findCountryById(int countryId);

    CountryDTO updateCountry(int countryId, CountryDTO country);

    void deleteCountry(int countryId);

}
