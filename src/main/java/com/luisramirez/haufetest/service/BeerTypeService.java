package com.luisramirez.haufetest.service;

import java.util.List;

import com.luisramirez.haufetest.dto.BeerTypeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface BeerTypeService {


    BeerTypeDTO createBeerType(BeerTypeDTO beerType);

    Page<BeerTypeDTO> getAllBeerTypes(Pageable pageable);

    BeerTypeDTO findBeerTypeById(int beerTypeId);

    BeerTypeDTO updateBeerType(int beerTypeId, BeerTypeDTO beerType);

    void deleteBeerType(int beerTypeId);

}
