package com.luisramirez.haufetest.service;

import java.io.IOException;
import java.nio.file.AccessDeniedException;

import com.luisramirez.haufetest.dto.BeerDTO;
import com.luisramirez.haufetest.dto.FilterDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface BeerService {


    BeerDTO createBeer(BeerDTO beer, String username) throws IOException;

    Page<BeerDTO> getAllBeers(Pageable pageable, FilterDTO filter) throws IOException;

    BeerDTO findBeerById(int beerId) throws IOException;

    BeerDTO updateBeer(int beerId, BeerDTO beer, String username) throws IOException;

    void deleteBeer(int beerId, String username) throws AccessDeniedException;

    BeerDTO uploadImage(int beerId, MultipartFile beerImage, String username) throws IOException;

}
