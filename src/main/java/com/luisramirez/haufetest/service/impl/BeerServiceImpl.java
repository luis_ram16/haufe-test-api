package com.luisramirez.haufetest.service.impl;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.luisramirez.haufetest.dto.BeerDTO;
import com.luisramirez.haufetest.dto.FilterDTO;
import com.luisramirez.haufetest.dto.PunkApiBeerDTO;
import com.luisramirez.haufetest.entity.BeerEntity;
import com.luisramirez.haufetest.exception.notfound.BeerNotFoundException;
import com.luisramirez.haufetest.exception.notupdated.BeerNotUpdatedException;
import com.luisramirez.haufetest.repository.BeerCustomRepository;
import com.luisramirez.haufetest.repository.BeerRepository;
import com.luisramirez.haufetest.service.BeerService;
import com.luisramirez.haufetest.service.mapper.BeerMapper;
import com.luisramirez.haufetest.service.mapper.PunkApiBeerMapper;
import com.luisramirez.haufetest.utils.FileUploadUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
public class BeerServiceImpl implements BeerService {

    @Autowired
    private BeerRepository beerRepository;

    @Autowired
    private BeerCustomRepository beerCustomRepository;

    @Autowired
    private BeerMapper beerMapper;

    @Autowired
    private PunkApiBeerMapper punkApiBeerMapper;

    public BeerDTO createBeer(BeerDTO beer, String username) throws IOException {
        if ("admin".equals(username) || username.equals(beer.getManufacturer().getName())){
            BeerEntity beerEntity = beerMapper.toEntity(beer);
            beerEntity.setCreatedDate(LocalDateTime.now());
            beerEntity.setUpdatedDate(LocalDateTime.now());
            return beerMapper.toDTO(beerRepository.save(beerEntity));
        }
        else {
            throw new AccessDeniedException("Can't create this beer because it's from a different manufacturer");
        }
    }

    public Page<BeerDTO> getAllBeers(Pageable pageable, FilterDTO filter) throws IOException {
        Page<BeerEntity> beerEntityPage = beerCustomRepository.findByFilter(filter, pageable);
        Page<BeerDTO> beerPage;
        if (beerEntityPage.getTotalElements() == 0) {
            beerPage = getInfoFromAPI(pageable, filter);

        } else {
            beerPage = new PageImpl<>(
                    beerMapper.toDTO(beerEntityPage.getContent()),
                    beerEntityPage.getPageable(),
                    beerEntityPage.getTotalElements());
        }
        return beerPage;
    }

    public BeerDTO findBeerById(int beerId) throws IOException {
        BeerEntity beerEntity = beerRepository.findById(beerId).orElseThrow(() -> new BeerNotFoundException(beerId));
        BeerDTO beer = beerMapper.toDTO(beerEntity);
        return beer;
    }

    public BeerDTO updateBeer(int beerId, BeerDTO beer, String username) throws IOException {
        BeerEntity beerEntityParam = beerMapper.toEntity(beer);
        beerEntityParam.setId(beerId);
        Optional<BeerEntity> beerEntity = beerRepository.findById(beerId);
        if (beerEntity.isPresent()) {
            if ("admin".equals(username) || username.equals(beerEntity.get().getManufacturer().getName())){
                beerEntityParam.setUpdatedDate(LocalDateTime.now());
                return beerMapper.toDTO(beerRepository.save(beerEntityParam));
            } else {
                throw new AccessDeniedException("Can't update this beer because it's from a different manufacturer");
            }
        } else {
            throw new BeerNotUpdatedException(beerId);
        }

    }

    public void deleteBeer(int beerId, String username) throws AccessDeniedException {
        Optional<BeerEntity> beerEntity = beerRepository.findById(beerId);
        if (beerEntity.isPresent()) {
            if ("admin".equals(username) || username.equals(beerEntity.get().getManufacturer().getName())) {
                beerRepository.deleteById(beerId);
            } else {
                throw new AccessDeniedException("Can't delete this beer because it's from a different manufacturer");
            }
        } else {
            throw new BeerNotFoundException(beerId);
        }
    }

    public BeerDTO uploadImage(int beerId, MultipartFile beerImage, String username) throws IOException {
        Optional<BeerEntity> beerEntityOptional = beerRepository.findById(beerId);
        if (beerEntityOptional.isPresent()) {
            BeerEntity beerEntity = beerEntityOptional.get();
            if ("admin".equals(username) || username.equals(beerEntity.getManufacturer().getName())){
                beerEntity.setUpdatedDate(LocalDateTime.now());
                beerEntity.setImageRoute(FileUploadUtil.saveFile("beer/"+beerId,beerId+".jpg",beerImage));
                return beerMapper.toDTO(beerRepository.save(beerEntity));
            } else {
                throw new AccessDeniedException("Can't update this beer because it's from a different manufacturer");
            }
        } else {
            throw new BeerNotUpdatedException(beerId);
        }
    }

    private Page<BeerDTO> getInfoFromAPI(Pageable pageable, FilterDTO filter){

        String uri = "https://api.punkapi.com/v2/beers";
        HttpHeaders headers = new HttpHeaders();
        headers.add("user-agent", "Application");
        HttpEntity<String> entity = new HttpEntity<>(headers);

        String params = "?page=" + (pageable.getPageNumber()+1) + "&per_page=" + pageable.getPageSize();

        String name = filter.getName();
        if (name != null && !"".equals(name)) {
            params += "&beer_name=" + name;
        }
        String manufacturerName = filter.getManufacturerName();
        if ((name == null || name == "") && manufacturerName != null && !"".equals(manufacturerName)) {
            params += "&beer_name=" + name;
        }
        Float graduation = filter.getGraduation();
        if (graduation != null && graduation != 0) {
            params += "&abv_gt=" + (graduation-0.1) + "&abv_lt=" + (graduation+0.1);
        }
        RestTemplate restTemplate = new RestTemplate();
        List<PunkApiBeerDTO> resultsFromApi;
        resultsFromApi = restTemplate.exchange(
                uri+params.replaceAll(" ","_"), HttpMethod.GET, entity, new ParameterizedTypeReference<List<PunkApiBeerDTO>>() {}).getBody();
        List<BeerDTO> beers = punkApiBeerMapper.toBeerDTO(resultsFromApi);
        Page<BeerDTO> beerPage = new PageImpl<>(beers, pageable, 500);
        return beerPage;
    }
}
