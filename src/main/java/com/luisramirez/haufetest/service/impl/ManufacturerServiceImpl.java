package com.luisramirez.haufetest.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import com.luisramirez.haufetest.dto.BeerTypeDTO;
import com.luisramirez.haufetest.dto.ManufacturerDTO;
import com.luisramirez.haufetest.entity.BeerTypeEntity;
import com.luisramirez.haufetest.entity.ManufacturerEntity;
import com.luisramirez.haufetest.exception.notfound.ManufacturerNotFoundException;
import com.luisramirez.haufetest.exception.notupdated.ManufacturerNotUpdatedException;
import com.luisramirez.haufetest.repository.ManufacturerRepository;
import com.luisramirez.haufetest.service.ManufacturerService;
import com.luisramirez.haufetest.service.mapper.ManufacturerMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ManufacturerServiceImpl implements ManufacturerService {

    @Autowired
    private ManufacturerRepository manufacturerRepository;

    @Autowired
    private ManufacturerMapper manufacturerMapper;

    public ManufacturerDTO createManufacturer(ManufacturerDTO manufacturer) {
        ManufacturerEntity manufacturerEntity = manufacturerMapper.toEntity(manufacturer);
        manufacturerEntity.setCreatedDate(LocalDateTime.now());
        manufacturerEntity.setUpdatedDate(LocalDateTime.now());
        return manufacturerMapper.toDTO(manufacturerRepository.save(manufacturerEntity));
    }

    public Page<ManufacturerDTO> getAllManufacturers(Pageable pageable) {
        Page<ManufacturerEntity> manufacturerEntityPage = manufacturerRepository.findAll(pageable);
        Page<ManufacturerDTO> manufacturerPage = new PageImpl<>(
                manufacturerMapper.toDTO(manufacturerEntityPage.getContent()),
                manufacturerEntityPage.getPageable(),
                manufacturerEntityPage.getTotalElements());
        return manufacturerPage;
    }

    public ManufacturerDTO findManufacturerById(int manufacturerId) {
        ManufacturerEntity manufacturerEntity = manufacturerRepository.findById(
                manufacturerId).orElseThrow(() -> new ManufacturerNotFoundException(manufacturerId));
        ManufacturerDTO manufacturer = manufacturerMapper.toDTO(manufacturerEntity);
        return manufacturer;
    }

    public ManufacturerDTO updateManufacturer(int manufacturerId, ManufacturerDTO manufacturer) {
        ManufacturerEntity manufacturerEntityParam = manufacturerMapper.toEntity(manufacturer);
        manufacturerEntityParam.setId(manufacturerId);
        return manufacturerRepository.findById(manufacturerId).map(manufacturerEntity -> {
            manufacturerEntityParam.setUpdatedDate(LocalDateTime.now());
            return manufacturerMapper.toDTO(manufacturerRepository.save(manufacturerEntityParam));
        }).orElseThrow(() -> new ManufacturerNotUpdatedException(manufacturerId));
    }

    public void deleteManufacturer(int manufacturerId) {
        manufacturerRepository.deleteById(manufacturerId);
    }
}
