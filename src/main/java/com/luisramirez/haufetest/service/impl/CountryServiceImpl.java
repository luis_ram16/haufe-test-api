package com.luisramirez.haufetest.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import com.luisramirez.haufetest.dto.BeerTypeDTO;
import com.luisramirez.haufetest.dto.CountryDTO;
import com.luisramirez.haufetest.entity.BeerTypeEntity;
import com.luisramirez.haufetest.entity.CountryEntity;
import com.luisramirez.haufetest.exception.notfound.CountryNotFoundException;
import com.luisramirez.haufetest.exception.notupdated.CountryNotUpdatedException;
import com.luisramirez.haufetest.repository.CountryRepository;
import com.luisramirez.haufetest.service.CountryService;
import com.luisramirez.haufetest.service.mapper.CountryMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CountryMapper countryMapper;

    public CountryDTO createCountry(CountryDTO country) {
        CountryEntity countryEntity = countryMapper.toEntity(country);
        countryEntity.setCreatedDate(LocalDateTime.now());
        countryEntity.setUpdatedDate(LocalDateTime.now());
        return countryMapper.toDTO(countryRepository.save(countryEntity));
    }

    public Page<CountryDTO> getAllCountries(Pageable pageable) {
        Page<CountryEntity> countryEntityPage = countryRepository.findAll(pageable);
        Page<CountryDTO> countryPage = new PageImpl<>(
                countryMapper.toDTO(countryEntityPage.getContent()),
                countryEntityPage.getPageable(),
                countryEntityPage.getTotalElements());
        return countryPage;
    }

    public CountryDTO findCountryById(int countryId) {
        CountryEntity countryEntity = countryRepository.findById(countryId).orElseThrow(() -> new CountryNotFoundException(countryId));
        CountryDTO country = countryMapper.toDTO(countryEntity);
        return country;
    }

    public CountryDTO updateCountry(int countryId, CountryDTO country) {
        CountryEntity countryEntityParam = countryMapper.toEntity(country);
        countryEntityParam.setId(countryId);
        return countryRepository.findById(countryId).map(countryEntity -> {
            countryEntityParam.setUpdatedDate(LocalDateTime.now());
            return countryMapper.toDTO(countryRepository.save(countryEntityParam));
        }).orElseThrow(() -> new CountryNotUpdatedException(countryId));
    }

    public void deleteCountry(int countryId) {
        countryRepository.deleteById(countryId);
    }
}
