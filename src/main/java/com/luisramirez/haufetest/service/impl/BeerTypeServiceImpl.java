package com.luisramirez.haufetest.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import com.luisramirez.haufetest.dto.BeerDTO;
import com.luisramirez.haufetest.dto.BeerTypeDTO;
import com.luisramirez.haufetest.entity.BeerEntity;
import com.luisramirez.haufetest.entity.BeerTypeEntity;
import com.luisramirez.haufetest.exception.notfound.BeerTypeNotFoundException;
import com.luisramirez.haufetest.exception.notupdated.BeerTypeNotUpdatedException;
import com.luisramirez.haufetest.repository.BeerTypeRepository;
import com.luisramirez.haufetest.service.BeerTypeService;
import com.luisramirez.haufetest.service.mapper.BeerTypeMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BeerTypeServiceImpl implements BeerTypeService {

    @Autowired
    private BeerTypeRepository beerTypeRepository;

    @Autowired
    private BeerTypeMapper beerTypeMapper;

    public BeerTypeDTO createBeerType(BeerTypeDTO beerType) {
        BeerTypeEntity beerTypeEntity = beerTypeMapper.toEntity(beerType);
        beerTypeEntity.setCreatedDate(LocalDateTime.now());
        beerTypeEntity.setUpdatedDate(LocalDateTime.now());
        return beerTypeMapper.toDTO(beerTypeRepository.save(beerTypeEntity));
    }

    public Page<BeerTypeDTO> getAllBeerTypes(Pageable pageable) {
        Page<BeerTypeEntity> beerTypeEntityPage = beerTypeRepository.findAll(pageable);
        Page<BeerTypeDTO> beerTypePage = new PageImpl<>(
                beerTypeMapper.toDTO(beerTypeEntityPage.getContent()),
                beerTypeEntityPage.getPageable(),
                beerTypeEntityPage.getTotalElements());
        return beerTypePage;
    }

    public BeerTypeDTO findBeerTypeById(int beerTypeId) {
        BeerTypeEntity beerTypeEntity = beerTypeRepository.findById(beerTypeId).orElseThrow(() -> new BeerTypeNotFoundException(beerTypeId));
        BeerTypeDTO beerType = beerTypeMapper.toDTO(beerTypeEntity);
        return beerType;
    }

    public BeerTypeDTO updateBeerType(int beerTypeId, BeerTypeDTO beerType) {
        BeerTypeEntity beerTypeEntityParam = beerTypeMapper.toEntity(beerType);
        beerTypeEntityParam.setId(beerTypeId);
        return beerTypeRepository.findById(beerTypeId).map(beerTypeEntity -> {
            beerTypeEntityParam.setUpdatedDate(LocalDateTime.now());
            return beerTypeMapper.toDTO(beerTypeRepository.save(beerTypeEntityParam));
        }).orElseThrow(() -> new BeerTypeNotUpdatedException(beerTypeId));
    }

    public void deleteBeerType(int beerTypeId) {
        beerTypeRepository.deleteById(beerTypeId);
    }
}
