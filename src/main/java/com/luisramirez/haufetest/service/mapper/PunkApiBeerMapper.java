package com.luisramirez.haufetest.service.mapper;

import java.util.List;

import com.luisramirez.haufetest.dto.BeerDTO;
import com.luisramirez.haufetest.dto.PunkApiBeerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PunkApiBeerMapper {

    @Mapping(source = "abv", target = "graduation")
    @Mapping(source = "image_url", target = "externalImageUrl")
    BeerDTO toBeerDTO(PunkApiBeerDTO punkAPIBeer);

    List<BeerDTO> toBeerDTO(List<PunkApiBeerDTO> punkAPIBeer);

}
