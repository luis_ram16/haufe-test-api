package com.luisramirez.haufetest.service.mapper;

import java.util.List;

import com.luisramirez.haufetest.dto.ManufacturerDTO;
import com.luisramirez.haufetest.entity.ManufacturerEntity;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

@Mapper(componentModel = "spring")
public interface ManufacturerMapper extends EntityDTOMapper<ManufacturerDTO, ManufacturerEntity> {

    ManufacturerDTO toDTO(ManufacturerEntity manufacturerEntity);

    List<ManufacturerDTO> toDTO(List<ManufacturerEntity> manufacturerEntities);

    ManufacturerEntity toEntity(ManufacturerDTO manufacturer);

    List<ManufacturerEntity> toEntity(List<ManufacturerDTO> manufacturers);

}
