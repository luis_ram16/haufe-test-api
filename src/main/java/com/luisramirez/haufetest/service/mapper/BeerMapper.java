package com.luisramirez.haufetest.service.mapper;

import java.io.IOException;
import java.util.List;

import com.luisramirez.haufetest.dto.BeerDTO;
import com.luisramirez.haufetest.entity.BeerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BeerMapper extends EntityDTOMapper<BeerDTO, BeerEntity> {

    @Mapping(source = "imageRoute", target = "image")
    BeerDTO toDTO(BeerEntity beerEntity) throws IOException;

    List<BeerDTO> toDTO(List<BeerEntity> beerEntities) throws IOException;

    BeerEntity toEntity(BeerDTO beer);

    List<BeerEntity> toEntity(List<BeerDTO> beers);

}
