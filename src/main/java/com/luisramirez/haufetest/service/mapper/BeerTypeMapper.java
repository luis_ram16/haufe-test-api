package com.luisramirez.haufetest.service.mapper;

import java.util.List;

import com.luisramirez.haufetest.dto.BeerTypeDTO;
import com.luisramirez.haufetest.entity.BeerTypeEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BeerTypeMapper extends EntityDTOMapper<BeerTypeDTO, BeerTypeEntity> {

    BeerTypeDTO toDTO(BeerTypeEntity beerTypeEntity);

    List<BeerTypeDTO> toDTO(List<BeerTypeEntity> beerTypeEntities);

    BeerTypeEntity toEntity(BeerTypeDTO beerType);

    List<BeerTypeEntity> toEntity(List<BeerTypeDTO> beerTypes);

}
