package com.luisramirez.haufetest.service.mapper;

import java.util.List;

import com.luisramirez.haufetest.dto.CountryDTO;
import com.luisramirez.haufetest.entity.CountryEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CountryMapper extends EntityDTOMapper<CountryDTO, CountryEntity> {

    CountryDTO toDTO(CountryEntity countryEntity);

    List<CountryDTO> toDTO(List<CountryEntity> countryEntity);

    CountryEntity toEntity(CountryDTO country);

    List<CountryEntity> toEntity(List<CountryDTO> countries);

}
