package com.luisramirez.haufetest.service.mapper;

import java.io.IOException;
import java.util.List;

/**
 * Contract for a generic dto to entity mapper.
 *
 * @param <D> - DTO type parameter.
 * @param <E> - Entity type parameter.
 */

public interface EntityDTOMapper<D, E> {
    E toEntity(D dto);

    D toDTO(E entity) throws IOException;

    List<E> toEntity(List<D> dtoList);

    List<D> toDto(List<E> entityList) throws IOException;

}
