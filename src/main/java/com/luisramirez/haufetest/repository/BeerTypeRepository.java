package com.luisramirez.haufetest.repository;

import com.luisramirez.haufetest.entity.BeerEntity;
import com.luisramirez.haufetest.entity.BeerTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BeerTypeRepository extends JpaRepository<BeerTypeEntity, Integer> {
}
