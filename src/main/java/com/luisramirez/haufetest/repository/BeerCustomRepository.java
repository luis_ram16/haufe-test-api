package com.luisramirez.haufetest.repository;

import com.luisramirez.haufetest.dto.FilterDTO;
import com.luisramirez.haufetest.entity.BeerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BeerCustomRepository {

    Page<BeerEntity> findByFilter(FilterDTO filter,
            Pageable pageable);

    }
