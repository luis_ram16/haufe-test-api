package com.luisramirez.haufetest.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.luisramirez.haufetest.dto.FilterDTO;
import com.luisramirez.haufetest.entity.BeerEntity;
import com.luisramirez.haufetest.repository.BeerCustomRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class BeerCustomRepositoryImpl implements BeerCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
        public Page<BeerEntity> findByFilter(FilterDTO filter,Pageable pageable) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<BeerEntity> criteriaQuery = criteriaBuilder.createQuery(BeerEntity.class);

        Root<BeerEntity> beerEntity = criteriaQuery.from(BeerEntity.class);
        List<Predicate> predicates = new ArrayList<>();
        Predicate predicate;
        String name = filter.getName();
        if (name != null && !"".equals(name)) {
            predicate = criteriaBuilder.like(beerEntity.get("name"), "%" + name + "%");
            predicates.add(predicate);
        }
        Float graduation = filter.getGraduation();
        if (graduation != null && graduation != 0) {
            predicate = criteriaBuilder.equal(beerEntity.get("graduation"), graduation);
            predicates.add(predicate);
        }
        String typeName = filter.getTypeName();
        if (typeName != null && !"".equals(typeName)) {
            predicate = criteriaBuilder.like(beerEntity.get("type").get("name"), "%" + typeName + "%");
            predicates.add(predicate);
        }
        String manufacturerName = filter.getManufacturerName();
        if (manufacturerName != null && !"".equals(manufacturerName)) {
            predicate = criteriaBuilder.like(beerEntity.get("manufacturer").get("name"), "%" + manufacturerName + "%");
            predicates.add(predicate);
        }
        String nationalityName = filter.getNationalityName();
        if (nationalityName != null && !"".equals(nationalityName)) {
            predicate = criteriaBuilder.like(beerEntity.get("manufacturer").get("nationality").get("name"), "%" + nationalityName + "%");
            predicates.add(predicate);
        }
        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        TypedQuery<BeerEntity> query = entityManager.createQuery(criteriaQuery);
        int totalCount = query.getResultList().size();
        query.setFirstResult(pageable.getPageNumber()*pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        List<BeerEntity> result = query.getResultList();
        return new PageImpl<>(result, pageable, totalCount);

    }
}
