package com.luisramirez.haufetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HaufetestApplication {

	public static void main(String[] args) {
		SpringApplication.run(HaufetestApplication.class, args);
	}

}
