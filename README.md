# haufe test api

Api made by Luis Ramirez as a Technical Challenge from Haufe Group.

# Build & Run up locally

## Pre Requisites

- Docker installed

## Building application

### Build with Maven

To build the application with Maven, run one of the following commands

```
    mvn clean install
```
```
    mvn clean package
```

## Run Application

Execute the following command:

```
    mvn spring-boot:run
```

Now if everything ran well, the application should be available, visiting for example: http://localhost:8080/api/beers

## Documentation

For documentation you have to go to: http://localhost:8080/swagger-ui.html


# Build & Run up locally with docker

## Pre Requisites

- MVN and JAVA installed and PATH configured

## Building application

### Build with Maven

To build the application with Maven, run the following commands

```
    mvn clean package
    docker build --tag=haufetest:latest .
```

## Run Application

Execute the following command changing -p8888 for any free port in your system

```
    docker run -p8888:8080 haufetest:latest
```

Now if everything ran well, the application should be available, visiting for example: http://localhost:8888/api/beers

## Using create, update, delete operations

The app is secured using spring security, so you would need to sign in using one of the three users added:

```
    admin:password
    polar:password
    heineken:password
```

# Technical decisions

## H2 database

As this is a small project I thought the best would be to using a h2 database for being created on demand and that the user doesn't have to create
the schema by himself

## Spring Data JPA

They main technical decision I made based on my previous knowledge was using Spring Data JPA for accessing the database and
make the calls a lot easier as the queries are quite simple, but also for having the search feature the best way I could think of was creating
a custom repository and using criteria builder for adding only the parameters that were present

## Mapping

The easiest way and with less boilerplate for making the mapping from entities to DTOs I know of is using mapstruct as the methods are auto created
based on the interface you create